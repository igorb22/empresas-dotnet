-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           8.0.18 - MySQL Community Server - GPL
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para myimbd
CREATE DATABASE IF NOT EXISTS `myimbd` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `myimbd`;

-- Copiando estrutura para tabela myimbd.genero
CREATE TABLE IF NOT EXISTS `genero` (
  `idGenero` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  PRIMARY KEY (`idGenero`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Copiando estrutura para tabela myimbd.filme
CREATE TABLE IF NOT EXISTS `filme` (
  `idFilme` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) NOT NULL,
  `idGenero` int(10) unsigned NOT NULL,
  PRIMARY KEY (`idFilme`),
  KEY `fk_Filme_Genero1_idx` (`idGenero`),
  CONSTRAINT `fk_Filme_Genero1` FOREIGN KEY (`idGenero`) REFERENCES `genero` (`idGenero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela myimbd.filme: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `filme` DISABLE KEYS */;
/*!40000 ALTER TABLE `filme` ENABLE KEYS */;

-- Copiando estrutura para tabela myimbd.elenco
CREATE TABLE IF NOT EXISTS `elenco` (
  `idElenco` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idFilme` int(10) unsigned NOT NULL,
  `nome` varchar(100) NOT NULL,
  `ator_diretor` enum('DIRETOR','ATOR') NOT NULL,
  PRIMARY KEY (`idElenco`),
  KEY `fk_pessoas_associadas_Filme1_idx` (`idFilme`),
  CONSTRAINT `fk_pessoas_associadas_Filme1` FOREIGN KEY (`idFilme`) REFERENCES `filme` (`idFilme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela myimbd.elenco: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `elenco` DISABLE KEYS */;
/*!40000 ALTER TABLE `elenco` ENABLE KEYS */;

-- Copiando estrutura para tabela myimbd.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `idUsuario` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_usuario` enum('ADMIN','USUARIO') NOT NULL,
  `nome` varchar(100) NOT NULL,
  `email` varchar(40) NOT NULL,
  `senha` varchar(200) NOT NULL,
  `ativo` tinyint(4) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela myimbd.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela myimbd.voto
CREATE TABLE IF NOT EXISTS `voto` (
  `idVoto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idUsuario` int(10) unsigned NOT NULL,
  `idFilme` int(10) unsigned NOT NULL,
  `voto` int(11) NOT NULL,
  PRIMARY KEY (`idVoto`),
  KEY `fk_voto_Usuario_idx` (`idUsuario`),
  KEY `fk_voto_Filme1_idx` (`idFilme`),
  CONSTRAINT `fk_voto_Filme1` FOREIGN KEY (`idFilme`) REFERENCES `filme` (`idFilme`),
  CONSTRAINT `fk_voto_Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `usuario` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela myimbd.voto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `voto` DISABLE KEYS */;
/*!40000 ALTER TABLE `voto` ENABLE KEYS */;

-- Copiando dados para a tabela myimbd.genero: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `genero` DISABLE KEYS */;
INSERT INTO `genero` (`idGenero`, `titulo`) VALUES
	(1, 'AÇÃO'),
	(2, 'AVENTURA'),
	(3, 'FICÇÃO'),
	(4, 'TERROR'),
	(5, 'COMÉDIA'),
	(6, 'INFANTIL');
/*!40000 ALTER TABLE `genero` ENABLE KEYS */;





/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
