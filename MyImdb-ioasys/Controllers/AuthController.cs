﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using MyImdb_ioasys.Model;
using MyImdb_ioasys.Model.ViewModel;
using MyImdb_ioasys.Service;
using Utils;

namespace teste_ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {

        private readonly IConfiguration _configuration;
        private readonly IUsuarioService _usuarioService;


        public AuthController(IConfiguration configuration, IUsuarioService service)
        {
            _configuration = configuration;
            _usuarioService = service;
        }

        [HttpPost]
        [Route("Login/")]
        public async Task<ActionResult> Login([FromBody] LoginViewModel request)
        {
            var user = await _usuarioService.ObterPorUsuarioSenha(request.Email, Criptography.GeneratePasswordHash(request.Senha));
            if (user != null)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.SerialNumber, user.IdUsuario.ToString()),
                        new Claim(ClaimTypes.Email, user.Email),
                        new Claim(ClaimTypes.NameIdentifier, user.Nome),
                        new Claim(ClaimTypes.Role, user.TipoUsuario)
                };


                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SecurityKey"]));

                var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(
                    issuer: "ProjetoImdbIoasys", 
                    audience: "ProjetoImdbIoasys",
                    claims: claims,
                    expires: DateTime.Now.AddDays(30),
                    signingCredentials: credentials
                );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }

            return BadRequest("Credenciais Invalidas");
        }

        [HttpPost]
        [Route("Cadastrarse/")]
        public async Task<ActionResult<Usuario>> Cadastrarse([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var usuarioEmailDuplicado = await _usuarioService.ObterPorEmail(usuario.Email);
                    if (usuarioEmailDuplicado != null)
                        return BadRequest("Este Email já está em uso, tente outro por email favor.");

                    usuario = await _usuarioService.Adicionar(usuario);

                    if (usuario != null)
                        return Ok(usuario);

                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }

            return BadRequest("dados inconsistente, tente novamente");
        }
    }
}
