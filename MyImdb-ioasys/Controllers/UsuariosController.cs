﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyImdb_ioasys.Model;
using MyImdb_ioasys.Service;

namespace teste_ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsuariosController : ControllerBase
    {
        private readonly IUsuarioService _usuarioService;

        public UsuariosController(IUsuarioService usuarioService)
        {
            _usuarioService = usuarioService;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ObterUsuarios/{pagina?}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<IEnumerable<Usuario>>> ObterUsuarios(int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1;

            var resultado = await _usuarioService.ObterUsuarios(pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("ObterPorId/{id}")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Usuario>> ObterPorId(int id)
        {
            var resultado = await _usuarioService.ObterUsuarioPorId(id);

            if (resultado == null)
                return BadRequest();

            return Ok(resultado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Adicionar/")]
        public async Task<ActionResult<Usuario>> Adicionar([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var usuarioEmailDuplicado = await _usuarioService.ObterPorEmail(usuario.Email);
                    if (usuarioEmailDuplicado != null)
                        return BadRequest("Este Email já está em uso, tente outro email por favor.");

                    usuario = await _usuarioService.Adicionar(usuario);

                    if (usuario != null)
                        return Ok(usuario);

                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }

            return BadRequest("dados inconsistente, tente novamente");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuario"></param>
        /// <returns></returns>
        [HttpPut]
        [Route("Editar/")]
        public async Task<ActionResult<Usuario>> Editar([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var usuarioEmailDuplicado = await _usuarioService.ObterPorEmail(usuario.Email);
                    if (usuarioEmailDuplicado != null && usuario.IdUsuario != usuarioEmailDuplicado.IdUsuario)
                        return BadRequest("Este Email já está em uso, tente outro email por favor.");

                    var usuarioLogado = _usuarioService.ObterUsuarioLogado((ClaimsIdentity)User.Identity);
                    var usuarioDesatualizado = await _usuarioService.ObterUsuarioPorId(usuario.IdUsuario);

                    if (usuarioDesatualizado != null && usuarioDesatualizado.TipoUsuario.Equals(Usuario.TIPO_ADMINISTRADOR) &&
                        usuarioLogado.TipoUsuario.Equals(Usuario.TIPO_USUARIO))
                        return StatusCode(401, "VocÊ não tem permissão para fazer esta operação no usuário especificado");

                    usuario = await _usuarioService.Editar(usuario);

                    if (usuario != null)
                        return Ok(usuario);

                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }

            return BadRequest("dados inconsistente, tente novamente");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Remover/{id}")]
        public async Task<ActionResult> Remover(int id)
        {
            try
            {
                var usuarioLogado = _usuarioService.ObterUsuarioLogado((ClaimsIdentity)User.Identity);
                var usuario = await _usuarioService.ObterUsuarioPorId(id);

                if (usuario != null && usuario.TipoUsuario.Equals(Usuario.TIPO_ADMINISTRADOR) &&
                    usuarioLogado.TipoUsuario.Equals(Usuario.TIPO_USUARIO))
                    return StatusCode(401, "VocÊ não tem permissão para fazer esta operação no usuário especificado");


                usuario = await _usuarioService.Remover(id);

                if (usuario != null)
                    return Ok("Registro removido com sucesso");

                return BadRequest();
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
