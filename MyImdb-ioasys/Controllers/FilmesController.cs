﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyImdb_ioasys.Model;
using MyImdb_ioasys.Model.ViewModel;
using MyImdb_ioasys.Service;
using MyImdb_ioasys.ViewModel;

namespace teste_ioasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FilmesController : ControllerBase
    {
        private readonly IFilmesService _filmeService;
        private readonly IVotoService _votoService;
        private readonly IUsuarioService _usuarioService;


        public FilmesController(IFilmesService filmeService, IVotoService votoService, IUsuarioService usuarioService)
        {
            _filmeService = filmeService;
            _votoService = votoService;
            _usuarioService = usuarioService;
        }

        // GET api/values
        [HttpGet]
        [Route("ObterTodos/{pagina?}")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterTodos(int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1;

            var resultado = await _filmeService.ObterTodos(pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        [HttpGet]
        [Route("ObterPorNome/{nome}/{pagina?}")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterPorNome(string nome, int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1;

            var resultado = await _filmeService.ObterPorNome(nome,pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        [HttpGet]
        [Route("ObterPorAtor/{ator}/{pagina?}")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterPorAtor(string ator, int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1;

            var resultado = await _filmeService.ObterPorAtor(ator,pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        [HttpGet]
        [Route("ObterPorDiretor/{diretor}/{pagina?}")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterPorDiretor(string diretor, int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1;

            var resultado = await _filmeService.ObterPorDiretor(diretor,pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        // GET api/values
        [HttpGet]
        [Route("ObterPorGenero/{genero}/{pagina?}")]
        public async Task<ActionResult<IEnumerable<FilmeViewModel>>> ObterPorGenero(string genero, int? pagina)
        {
            if (!pagina.HasValue || pagina.Value <= 0)
                pagina = 1; 

            var resultado = await _filmeService.ObterPorGenero(genero,pagina.Value);

            if (resultado.Count == 0)
                return NoContent();

            return Ok(resultado);
        }

        [HttpPost]
        [Route("Adicionar/")]
        [Authorize(Roles = "ADMIN")]
        public async Task<ActionResult<Filme>> Adicionar([FromBody] Filme filme)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    filme = await _filmeService.Adicionar(filme);

                    if (filme != null)
                        return Ok(filme);

                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }

            return BadRequest("dados inconsistente, tente novamente");
        }

        [HttpPost]
        [Route("Votar/")]
        [Authorize(Roles = "USUARIO")]
        public async Task<ActionResult<VotoViewModel>> Votar([FromBody] VotoViewModel votoViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var voto = new Voto { IdVoto = 0, IdFilme = votoViewModel.IdFilme, Voto1 = votoViewModel.Voto, IdUsuario = _usuarioService.ObterUsuarioLogado((ClaimsIdentity)User.Identity).IdUsuario };

                    var filme  = await _filmeService.ObterPorId(voto.IdFilme);
                    if (filme == null)
                        return BadRequest("Filme informado não existe ou não foi encontrado");

                    voto = await _votoService.Adicionar(voto);

                    if (voto != null)
                        return Ok(voto);

                    return BadRequest();
                }
                catch (Exception e)
                {
                    return StatusCode(500, e.Message);
                }
            }

            return BadRequest("dados inconsistente, tente novamente");
        }
    }
}
