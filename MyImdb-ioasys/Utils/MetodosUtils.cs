﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Utils
{
    public static class MetodosUtils
    {
        public static string CleanString(string stringPoluida) => Regex.Replace(stringPoluida, "[^0-9a-zA-Z]+", "");

    }
}
