﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyImdb_ioasys.Model
{
    public class Filme
    {
        public Filme()
        {
            Elenco = new List<Elenco>();
            Voto = new HashSet<Voto>();
        }

        public int IdFilme { get; set; }
        [JsonRequired]
        [StringLength(150, ErrorMessage = "Máximo são 100 caracteres")]
        public string Nome { get; set; }
        [JsonRequired]
        public int IdGenero { get; set; }

        [JsonIgnore]
        public virtual Genero IdGeneroNavigation { get; set; }
        public virtual ICollection<Elenco> Elenco { get; set; }
        [JsonIgnore]
        public virtual ICollection<Voto> Voto { get; set; }

    }
}
