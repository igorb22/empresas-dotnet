﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Model.ViewModel
{
    public class VotoViewModel
    {
        [JsonRequired]
        [Range(1, Double.PositiveInfinity)]
        public int IdFilme { get; set; }
        [JsonRequired]
        [Range(0, 4,ErrorMessage = "O valor para voto deve ser entre 0 e 4")]
        public int Voto { get; set; }
    }
}
