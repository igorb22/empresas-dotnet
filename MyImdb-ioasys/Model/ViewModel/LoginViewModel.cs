﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Model.ViewModel
{
    public class LoginViewModel
    {
        [JsonRequired]
        public string Email { get; set; }
        [JsonRequired]
        public string Senha { get; set; }
    }
}
