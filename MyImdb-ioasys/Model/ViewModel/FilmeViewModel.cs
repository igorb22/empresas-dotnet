﻿using Microsoft.AspNetCore.Mvc;
using MyImdb_ioasys.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace MyImdb_ioasys.ViewModel
{
    public class FilmeViewModel
    {
        public FilmeViewModel()
        {
            Elenco = new List<Elenco>();
        }


        public int IdFilme { get; set; }
        public string Nome { get; set; }
        public int IdGenero { get; set; }
        [NotMapped]
        public string Genero { get; set; }
        [NotMapped]
        public float? Media { get; set; }
        public List<Elenco> Elenco { get; set; }


    }
}
