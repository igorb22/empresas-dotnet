﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyImdb_ioasys.Model
{
    public partial class Usuario
    {
        public static string TIPO_ADMINISTRADOR = "ADMIN";
        public static string TIPO_USUARIO = "USUARIO";

        public Usuario()
        {
            Voto = new HashSet<Voto>();
        }
        public int IdUsuario { get; set; }
        [JsonRequired]
        public string TipoUsuario { get; set; }
        [JsonRequired]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Máximo de 100 caracteres, no mínimo 3")]
        public string Nome { get; set; }
        [JsonRequired]
        [EmailAddress(ErrorMessage = "E-mail em formato inválido.")]
        public string Email { get; set; }
        [JsonRequired]
        [StringLength(8, MinimumLength = 4, ErrorMessage = "Máximo de 8 caracteres e no mínimo 4")]
        public string Senha { get; set; }
        public bool Ativo { get; set; }
        
        [JsonIgnore]
        public virtual ICollection<Voto> Voto { get; set; }
    }
}
