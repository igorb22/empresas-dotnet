﻿using System;
using System.Collections.Generic;

namespace MyImdb_ioasys.Model
{
    public partial class Genero
    {
        public Genero()
        {
            Filme = new HashSet<Filme>();
        }

        public int IdGenero { get; set; }
        public string Titulo { get; set; }

        public virtual ICollection<Filme> Filme { get; set; }
    }
}
