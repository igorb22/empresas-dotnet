﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MyImdb_ioasys.Model
{
    public partial class myImbdContext : DbContext
    {
        public myImbdContext()
        {
        }

        public myImbdContext(DbContextOptions<myImbdContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Filme> Filme { get; set; }
        public virtual DbSet<Genero> Genero { get; set; }
        public virtual DbSet<Elenco> Elenco { get; set; }
        public virtual DbSet<Usuario> Usuario { get; set; }
        public virtual DbSet<Voto> Voto { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Filme>(entity =>
            {
                entity.HasKey(e => e.IdFilme);

                entity.ToTable("filme", "myimbd");

                entity.HasIndex(e => e.IdGenero)
                    .HasName("fk_Filme_Genero1_idx");

                entity.Property(e => e.IdFilme)
                    .HasColumnName("idFilme")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdGenero)
                    .HasColumnName("idGenero")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdGeneroNavigation)
                    .WithMany(p => p.Filme)
                    .HasForeignKey(d => d.IdGenero)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_Filme_Genero1");
            });

            modelBuilder.Entity<Genero>(entity =>
            {
                entity.HasKey(e => e.IdGenero);

                entity.ToTable("genero", "myimbd");

                entity.Property(e => e.IdGenero)
                    .HasColumnName("idGenero")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Titulo)
                    .IsRequired()
                    .HasColumnName("titulo")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Elenco>(entity =>
            {
                entity.HasKey(e => e.IdElenco);

                entity.ToTable("elenco", "myimbd");

                entity.HasIndex(e => e.IdFilme)
                    .HasName("fk_pessoas_associadas_Filme1_idx");

                entity.Property(e => e.IdElenco)
                    .HasColumnName("idElenco")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AtorDiretor)
                    .IsRequired()
                    .HasColumnName("ator_diretor")
                    .HasColumnType("enum('DIRETOR','ATOR')");

                entity.Property(e => e.IdFilme)
                    .HasColumnName("idFilme")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFilmeNavigation)
                    .WithMany(p => p.Elenco)
                    .HasForeignKey(d => d.IdFilme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_pessoas_associadas_Filme1");
            });

            modelBuilder.Entity<Usuario>(entity =>
            {
                entity.HasKey(e => e.IdUsuario);

                entity.ToTable("usuario", "myimbd");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("idUsuario")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasColumnName("nome")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Senha)
                    .IsRequired()
                    .HasColumnName("senha")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Ativo)
                  .HasColumnName("ativo")
                  .HasColumnType("tinyint(4)");

                entity.Property(e => e.TipoUsuario)
                    .IsRequired()
                    .HasColumnName("tipo_usuario")
                    .HasColumnType("enum('ADMIN','COMUM')");
            });

            modelBuilder.Entity<Voto>(entity =>
            {
                entity.HasKey(e => e.IdVoto);

                entity.ToTable("voto", "myimbd");

                entity.HasIndex(e => e.IdFilme)
                    .HasName("fk_voto_Filme1_idx");

                entity.HasIndex(e => e.IdUsuario)
                    .HasName("fk_voto_Usuario_idx");

                entity.Property(e => e.IdVoto)
                    .HasColumnName("idVoto")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdFilme)
                    .HasColumnName("idFilme")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.IdUsuario)
                    .HasColumnName("idUsuario")
                    .HasColumnType("int(10) unsigned");

                entity.Property(e => e.Voto1)
                    .HasColumnName("voto")
                    .HasColumnType("int(11)");

                entity.HasOne(d => d.IdFilmeNavigation)
                    .WithMany(p => p.Voto)
                    .HasForeignKey(d => d.IdFilme)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_voto_Filme1");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Voto)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_voto_Usuario");
            });
        }
    }
}
