﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyImdb_ioasys.Model
{
    public partial class Elenco
    {
        public static string TIPO_ATOR = "ATOR";
        public static string TIPO_DIRETOR = "DIRETOR";

        public int IdElenco { get; set; }
        [JsonIgnore]
        public int IdFilme { get; set; }
        [JsonRequired]
        [StringLength(100, ErrorMessage = "Máximo são 100 caracteres")]
        public string Nome { get; set; }
        [JsonRequired]
        public string AtorDiretor { get; set; }

        [JsonIgnore]
        public virtual Filme IdFilmeNavigation { get; set; }
    }
}
