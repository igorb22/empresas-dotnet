﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MyImdb_ioasys.Model
{
    public partial class Voto
    {
        public int IdVoto { get; set; }
        [JsonIgnore]
        public int IdUsuario { get; set; }
        [JsonRequired]
        public int IdFilme { get; set; }
        [JsonRequired]
        [Range(0, 4,ErrorMessage = "O valor para voto deve ser entre 0 e 4")]
        public int Voto1 { get; set; }

        [JsonIgnore]
        public virtual Filme IdFilmeNavigation { get; set; }
        [JsonIgnore]
        public virtual Usuario IdUsuarioNavigation { get; set; }
    }
}
