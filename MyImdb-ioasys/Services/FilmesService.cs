﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using MyImdb_ioasys.Model;
using MyImdb_ioasys.Utils;
using MyImdb_ioasys.ViewModel;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Service
{
    public class FilmesService : IFilmesService
    {
        private readonly myImbdContext _context;
        public FilmesService(myImbdContext context)
        {
            _context = context;
        }

        public async Task<Filme> Adicionar(Filme filme)
        {

            using (var transaction = _context.Database.BeginTransaction())
            {

                try
                {
                    var elenco = filme.Elenco.ToList();
                    filme.Elenco = new HashSet<Elenco>();
                    filme.IdFilme = 0;

                    await _context.AddAsync(filme);
                    _context.SaveChanges();

                    if (elenco.Count > 0)
                    {
                        IElencoService _elencoService = new ElencoService(_context);

                        elenco.ForEach(e =>
                        {
                            e.IdFilme = filme.IdFilme;
                            _elencoService.Adicionar(e);
                        });

                    }

                    filme.Elenco = elenco;

                    transaction.Commit();

                    return filme;
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    throw e;
                }
            }
        }

        public async Task<List<FilmeViewModel>> ObterTodos(int pagina)
        => await _context
            .Filme
            .Include(f => f.IdGeneroNavigation)
            .Skip((pagina - 1) * 5)
            .Take(5)
            .Select(f => new FilmeViewModel
            {
                IdFilme = f.IdFilme,
                Nome = f.Nome,
                IdGenero = f.IdGenero,
                Genero = f.IdGeneroNavigation.Titulo,
                Media = _context.Voto.Count(v => v.IdFilme == f.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == f.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == f.IdFilme)) : 0,
                Elenco = _context.Elenco.Where(pa => pa.IdFilme == f.IdFilme).Select(pa => new Elenco
                {
                    IdElenco = pa.IdElenco,
                    IdFilme = pa.IdFilme,
                    Nome = pa.Nome,
                    AtorDiretor = pa.AtorDiretor
                }).ToList(),
            }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome).ToListAsync();


        public async Task<List<FilmeViewModel>> ObterPorNome(string nome, int pagina)
        => await _context
           .Filme
           .Include(f => f.IdGeneroNavigation)
           .Skip((pagina - 1) * 5)
           .Take(5)
           .Where(f => MetodosUtils.CleanString(f.Nome).ToUpper().Contains(MetodosUtils.CleanString(nome.ToUpper())))
           .Select(f => new FilmeViewModel
           {
               IdFilme = f.IdFilme,
               Nome = f.Nome,
               IdGenero = f.IdGenero,
               Genero = f.IdGeneroNavigation.Titulo,
               Media = _context.Voto.Count(v => v.IdFilme == f.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == f.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == f.IdFilme)) : 0,
               Elenco = _context.Elenco.Where(pa => pa.IdFilme == f.IdFilme).Select(pa => new Elenco
               {
                   IdElenco = pa.IdElenco,
                   IdFilme = pa.IdFilme,
                   Nome = pa.Nome,
                   AtorDiretor = pa.AtorDiretor
               }).ToList(),
              
           }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome).ToListAsync();

        public async Task<List<FilmeViewModel>> ObterPorDiretor(string diretor, int pagina)
       => await _context
          .Elenco
            .Include(pa => pa.IdFilmeNavigation)
            .Include(pa => pa.IdFilmeNavigation.IdGeneroNavigation)
            .Where(pa => MetodosUtils.CleanString(pa.Nome).ToUpper().Contains(MetodosUtils.CleanString(diretor.ToUpper()))
            && pa.AtorDiretor.Equals(Elenco.TIPO_DIRETOR))
             .Select(pa => new FilmeViewModel
             {
                 IdFilme = pa.IdFilmeNavigation.IdFilme,
                 Nome = pa.IdFilmeNavigation.Nome,
                 IdGenero = pa.IdFilmeNavigation.IdGenero,
                 Genero = pa.IdFilmeNavigation.IdGeneroNavigation.Titulo,
                 Media = _context.Voto.Count(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme)) : 0,
                 Elenco = _context.Elenco.Where(pa1 => pa1.IdFilme == pa.IdFilme).Select(pa1 => new Elenco
                 {
                     IdElenco = pa1.IdElenco,
                     IdFilme = pa1.IdFilme,
                     Nome = pa1.Nome,
                     AtorDiretor = pa1.AtorDiretor
                 }).ToList(),
               
             }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome)
               .Skip((pagina - 1) * 5)
               .Take(5)
               .ToListAsync();

        public async Task<List<FilmeViewModel>> ObterPorGenero(string genero, int pagina)
      => await _context
         .Filme
         .Include(f => f.IdGeneroNavigation)
         .Where(f => MetodosUtils.CleanString(f.IdGeneroNavigation.Titulo).ToUpper().Contains(MetodosUtils.CleanString(genero.ToUpper())))
         .Select(f => new FilmeViewModel
         {
             IdFilme = f.IdFilme,
             Nome = f.Nome,
             IdGenero = f.IdGenero,
             Genero = f.IdGeneroNavigation.Titulo,
             Media = _context.Voto.Count(v => v.IdFilme == f.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == f.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == f.IdFilme)) : 0,
             Elenco = _context.Elenco.Where(pa => pa.IdFilme == f.IdFilme).Select(pa => new Elenco
             {
                 IdElenco = pa.IdElenco,
                 IdFilme = pa.IdFilme,
                 Nome = pa.Nome,
                 AtorDiretor = pa.AtorDiretor
             }).ToList(),
         }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome)
            .Skip((pagina - 1) * 5)
            .Take(5)
            .ToListAsync();

        public async Task<FilmeViewModel> ObterPorId(int id)
        => await _context
           .Filme
           .Include(f => f.IdGeneroNavigation)
           .Where(f => f.IdFilme == id)
           .Select(f => new FilmeViewModel
           {
               IdFilme = f.IdFilme,
               Nome = f.Nome,
               IdGenero = f.IdGenero,
               Genero = f.IdGeneroNavigation.Titulo,
               Media = _context.Voto.Count(v => v.IdFilme == f.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == f.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == f.IdFilme)) : 0,
               Elenco = _context.Elenco.Where(pa => pa.IdFilme == f.IdFilme).Select(pa => new Elenco
               {
                   IdElenco = pa.IdElenco,
                   IdFilme = pa.IdFilme,
                   Nome = pa.Nome,
                   AtorDiretor = pa.AtorDiretor
               }).ToList(),
      
           }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome).FirstOrDefaultAsync();


        public async Task<List<FilmeViewModel>> ObterPorAtor(string ator, int pagina)
       => await _context
          .Elenco
            .Include(pa => pa.IdFilmeNavigation)
            .Include(pa => pa.IdFilmeNavigation.IdGeneroNavigation)
            .Where(pa => MetodosUtils.CleanString(pa.Nome).ToUpper().Contains(MetodosUtils.CleanString(ator.ToUpper())) && pa.AtorDiretor.Equals(Elenco.TIPO_ATOR))
             .Select(pa => new FilmeViewModel
             {
                 IdFilme = pa.IdFilmeNavigation.IdFilme,
                 Nome = pa.IdFilmeNavigation.Nome,
                 IdGenero = pa.IdFilmeNavigation.IdGenero,
                 Genero = pa.IdFilmeNavigation.IdGeneroNavigation.Titulo,
                 Media = _context.Voto.Count(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme) > 0 ? (_context.Voto.Where(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme).Sum(v => v.Voto1) / _context.Voto.Count(v => v.IdFilme == pa.IdFilmeNavigation.IdFilme)) : 0,
                 Elenco = _context.Elenco.Where(pa1 => pa1.IdFilme == pa.IdFilme).Select(pa1 => new Elenco
                 {
                     IdElenco = pa1.IdElenco,
                     IdFilme = pa1.IdFilme,
                     Nome = pa1.Nome,
                     AtorDiretor = pa1.AtorDiretor
                 }).ToList(),
             }).OrderByDescending(f => f.Media).OrderBy(f => f.Nome)
               .Skip((pagina - 1) * 5)
               .Take(5)
               .ToListAsync();

    }
}
