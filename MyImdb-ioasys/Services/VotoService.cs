﻿using MyImdb_ioasys.Model;
using MyImdb_ioasys.Model.ViewModel;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Service
{
    public class VotoService : IVotoService
    {
        private readonly myImbdContext _context;
        public VotoService(myImbdContext context)
        {
            _context = context;
        }

        public async Task<Voto> Adicionar(Voto voto)
        {
            try
            {
                await _context.AddAsync(voto);
                var save = (_context.SaveChanges() == 1);

                if (save)
                {
                    return voto;
                }

                return null;
            } 
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
