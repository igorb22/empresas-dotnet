﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyImdb_ioasys.Model;
using Utils;

namespace MyImdb_ioasys.Service
{
    public class UsuarioService : IUsuarioService
    {
        private readonly myImbdContext _context;
        public UsuarioService(myImbdContext context)
        {
            _context = context;
        }

        public async Task<Usuario> Remover(int id)
        {
            try
            {
                var usuario = _context.Usuario.Where(u => u.IdUsuario == id).FirstOrDefault();
                
                if (usuario != null)
                {
                    usuario.Ativo = false;
                    return await Editar(usuario);
                }

                return null;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<Usuario> Editar(Usuario usuario)
        {
            try
            {
                var x = await _context.Usuario.Where(u => u.IdUsuario == usuario.IdUsuario).FirstOrDefaultAsync();
                _context.Entry(x).State = EntityState.Detached;

                if (x != null)
                {
                    _context.Update(usuario);
                    bool save = _context.SaveChanges() == 1;

                    if (save)
                        return usuario;
                }

                return null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public async Task<Usuario> Adicionar(Usuario usuario)
        {
            try
            {
                usuario.Senha = Criptography.GeneratePasswordHash(usuario.Senha); 
                usuario.IdUsuario = 0;

                await _context.AddAsync(usuario);
                bool save = _context.SaveChanges() == 1;

                if (save)
                    return usuario;

                return null;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public async Task<Usuario> ObterPorUsuarioSenha(string email, string senha)
         => await _context.Usuario
                .Where(u => u.Email.Equals(email) && u.Senha.Equals(senha) && u.Ativo)
                .Select(u => new Usuario
                {
                    IdUsuario = u.IdUsuario,
                    TipoUsuario = u.TipoUsuario,
                    Nome = u.Nome,
                    Email = u.Email,
                    Senha = u.Senha,
                    Ativo = u.Ativo
                }).FirstOrDefaultAsync();

        public async Task<Usuario> ObterPorEmail(string email)
       => await _context.Usuario
              .Where(u => u.Email.Equals(email))
              .Select(u => new Usuario
              {
                  IdUsuario = u.IdUsuario,
                  TipoUsuario = u.TipoUsuario,
                  Nome = u.Nome,
                  Email = u.Email,
                  Senha = u.Senha,
                  Ativo = u.Ativo
              }).FirstOrDefaultAsync();

        public async Task<Usuario> ObterUsuarioPorId(int id)
         => await _context.Usuario
                .Where(u => u.IdUsuario == id)
                .Select(u => new Usuario
                {
                    IdUsuario = u.IdUsuario,
                    TipoUsuario = u.TipoUsuario,
                    Nome = u.Nome,
                    Email = u.Email,
                    Senha = u.Senha,
                    Ativo = u.Ativo
                }).FirstOrDefaultAsync();

        public async Task<List<Usuario>> ObterUsuarios(int pagina)
         => await _context
            .Usuario
            .Where(u => u.TipoUsuario.Equals("USUARIO"))
            .Skip((pagina - 1 )* 5 )
            .Take(5)
            .Include(u => u.Voto)
            .Select(u => new Usuario
            {
                IdUsuario = u.IdUsuario,
                TipoUsuario = u.TipoUsuario,
                Nome = u.Nome,
                Email = u.Email,
                Senha = u.Senha,
                Ativo = u.Ativo
            }).OrderBy(u => u.Nome).ToListAsync();


        public Usuario ObterUsuarioLogado(ClaimsIdentity claimsIdentity)
        {
            var usuario = new Usuario
            {
                IdUsuario = int.Parse(claimsIdentity.Claims.Where(s => s.Type == ClaimTypes.SerialNumber).Select(s => s.Value).FirstOrDefault()),
                Email = claimsIdentity.Claims.Where(s => s.Type == ClaimTypes.Email).Select(s => s.Value).FirstOrDefault(),
                Nome = claimsIdentity.Claims.Where(s => s.Type == ClaimTypes.NameIdentifier).Select(s => s.Value).FirstOrDefault(),
                TipoUsuario = claimsIdentity.Claims.Where(s => s.Type == ClaimTypes.Role).Select(s => s.Value).FirstOrDefault(),

            };

            return usuario;
        }

    }
}
