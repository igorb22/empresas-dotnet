﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyImdb_ioasys.Model;

namespace MyImdb_ioasys.Service
{
    public class ElencoService : IElencoService
    {
        private readonly myImbdContext _context;
        public ElencoService(myImbdContext context)
        {
            _context = context;
        }

        public async Task<bool> Adicionar(List<Elenco> elenco)
        {
            try
            {
                await _context.Elenco.AddRangeAsync(elenco);
                bool save = _context.SaveChanges() == 1;

                return save;
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public async Task<Elenco> Adicionar(Elenco elenco)
        {
            try
            {
                elenco.IdElenco = 0;
                await _context.Elenco.AddAsync(elenco);
                bool save = _context.SaveChanges() == 1;
                _context.Entry(elenco).State = EntityState.Detached;

                if (save)
                    return elenco;

                return null;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
