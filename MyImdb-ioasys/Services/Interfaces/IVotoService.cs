﻿using MyImdb_ioasys.Model;
using MyImdb_ioasys.Model.ViewModel;
using System.Threading.Tasks;

namespace MyImdb_ioasys.Service
{
    public interface IVotoService
    {
       Task<Voto> Adicionar(Voto voto);
    }
}
