﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb_ioasys.Model;

namespace MyImdb_ioasys.Service
{
    public interface IElencoService
    {
        Task<bool> Adicionar(List<Elenco> elenco);
        Task<Elenco> Adicionar(Elenco elenco);

    }
}
