﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MyImdb_ioasys.Model;
using MyImdb_ioasys.ViewModel;

namespace MyImdb_ioasys.Service
{
    public interface IFilmesService
    {
        Task<Filme> Adicionar(Filme filme);

        Task<List<FilmeViewModel>> ObterTodos(int pagina);
        Task<List<FilmeViewModel>> ObterPorNome(string nome, int pagina);
        Task<List<FilmeViewModel>> ObterPorDiretor(string diretor, int pagina);
        Task<List<FilmeViewModel>> ObterPorGenero(string genero, int pagina);
        Task<List<FilmeViewModel>> ObterPorAtor(string ator, int pagina);

        Task<FilmeViewModel> ObterPorId(int id);

    }
}
