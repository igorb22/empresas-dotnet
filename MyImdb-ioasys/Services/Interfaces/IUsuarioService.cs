﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using MyImdb_ioasys.Model;

namespace MyImdb_ioasys.Service
{
    public interface IUsuarioService
    {
        Task<Usuario> Adicionar(Usuario usuario);
        Task<Usuario> Editar(Usuario usuario);
        Task<Usuario> Remover(int id);
        Task<Usuario> ObterUsuarioPorId(int id);
        Usuario ObterUsuarioLogado(ClaimsIdentity claimsIdentity);
        Task<Usuario> ObterPorEmail(string email);
        Task<List<Usuario>> ObterUsuarios(int pagina);
        Task<Usuario> ObterPorUsuarioSenha(string email,string senha);

    }
}
